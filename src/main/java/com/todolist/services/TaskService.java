package com.todolist.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.todolist.models.Task;
import com.todolist.repositories.TaskRepository;

@Service
public class TaskService {
    @Autowired
    private TaskRepository taskRepository;

    public List<Task> allTask() {
        return taskRepository.findAll();
    }

    public Task findTaskById(long taskId) {
        return taskRepository.findOne(taskId);
    }

    public Task saveTask(Task task) {
        return taskRepository.save(task);
    }

    public void deleteTask(long taskId) {
        taskRepository.removeById(taskId);
    }
}
