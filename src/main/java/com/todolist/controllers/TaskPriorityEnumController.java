package com.todolist.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.todolist.models.Priority;

@RestController
public class TaskPriorityEnumController {
	
	@RequestMapping(value = "/populateTasksPriorities", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody
	List<Priority> populateTasksPriorities(){
		return new ArrayList<Priority>(Arrays.asList(Priority.values()));
	}
}
