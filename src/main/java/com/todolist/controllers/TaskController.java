/*
 *  This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.todolist.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.todolist.models.Task;
import com.todolist.services.TaskService;

@RestController
@RequestMapping("/tasks")
public class TaskController {
	
	@Autowired 
	private TaskService taskService;
	
	@RequestMapping(method = RequestMethod.GET)
	public List<Task> tasks(){	
		return taskService.allTask();
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/{id}")
	public Task getTaskById(@PathVariable("id") Long id){
	    return taskService.findTaskById(id);
	}

	@RequestMapping(method = RequestMethod.POST)
	public Task saveTask(@RequestBody Task task){
		return taskService.saveTask(task);
	}
	
	@RequestMapping(method = RequestMethod.DELETE, value="/{id}")
	public void deleteTask(@PathVariable("id") Long id){
		taskService.deleteTask(id);
	}
	
	@RequestMapping(method = RequestMethod.PUT, value="/{id}")
	public Task editTask(@PathVariable("id") long id, @RequestBody Task editedTask){
		return taskService.saveTask(editedTask);
	}
}