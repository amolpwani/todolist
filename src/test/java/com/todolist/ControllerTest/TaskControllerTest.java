package com.todolist.ControllerTest;

import static org.assertj.core.api.Assertions.assertThat;

import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.todolist.models.Priority;
import com.todolist.models.Task;
import com.todolist.services.TaskService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TaskControllerTest {

	@LocalServerPort
	private int port = 8080;

	private URL base;

	@Autowired
	private TestRestTemplate template;

	@Autowired
	private TaskService taskService;

	private List<Long> testEntitiesId = new ArrayList<>();
	
	@SuppressWarnings("deprecation")
	private Date taskDate1 = new Date(118, 1, 27);

	@Before
	public void setUp() throws Exception {
		Task task1 = taskService.saveTask(buildTask("Task1", "Activity1"));
		Task task2 = taskService.saveTask(buildTask("Task2", "Activity2"));
		testEntitiesId.add(task1.getId());
		testEntitiesId.add(task2.getId());
		this.base = new URL("http://localhost:" + port);
	}

	@After
	public void tearDown() {
		for (Long taskId : testEntitiesId) {
			taskService.deleteTask(taskId);
		}
	}

	private Task buildTask(final String name, String activity) {
		return new Task(name, taskDate1, activity, Priority.HIGH, false);
	}

	@Test
	public void shouldRetrieveOneTask() throws Exception {
		//Act
		ParameterizedTypeReference<Task> returnType = new ParameterizedTypeReference<Task>() {
		};
		ResponseEntity<Task> actualResponse =  template.exchange(
				base.toString() + "/tasks/" + testEntitiesId.get(0),
				HttpMethod.GET,
				null,
				returnType);

		//Assert
		assertThat(actualResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
		Task expectedTask = new Task("Task1", taskDate1, "Activity1", Priority.HIGH, false);

		expectedTask.setId(testEntitiesId.get(0));
		assertThat(actualResponse.getBody()).isEqualToComparingFieldByField(expectedTask);
	}

	@Test
	public void shouldRetrieveAllTask() {
		//Act
		ParameterizedTypeReference<List<Task>> returnType = new ParameterizedTypeReference<List<Task>>() {
		};

		ResponseEntity<List<Task>> actualResponse =  template.exchange(
				base.toString() + "/tasks",
				HttpMethod.GET,
				null,
				returnType);
		//Assert
		assertThat(actualResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
	}

	@Test
	public void shouldUpdateTask() throws Exception {
		//Act
		ParameterizedTypeReference<Task> returnType = new ParameterizedTypeReference<Task>() {
		};

		Task updateTask = new Task("Task1", taskDate1, "Activity1", Priority.HIGH, false);
		updateTask.setId(testEntitiesId.get(0));

		ResponseEntity<Task> actualResponse =  template.exchange(
				base.toString() + "/tasks/" + testEntitiesId.get(0),
				HttpMethod.PUT,
				new HttpEntity<Task>(updateTask, new HttpHeaders()),
				Task.class,
				returnType);

		//Assert
		assertThat(actualResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
		Task expectedTask = new Task("Task1", taskDate1, "Activity1", Priority.HIGH, false);
		expectedTask.setId(testEntitiesId.get(0));
		assertThat(actualResponse.getBody()).isEqualToComparingFieldByField(expectedTask);
	}

	@Test
	public void shouldDeleteTaskById() {
		//Act
		ParameterizedTypeReference<List<Task>> returnType = new ParameterizedTypeReference<List<Task>>() {
		};

		template.delete(
				base.toString() + "/tasks/"+ testEntitiesId.get(0));

		testEntitiesId.remove(testEntitiesId.get(0));

		ResponseEntity<List<Task>> actualResponse =  template.exchange(
				base.toString() + "/tasks",
				HttpMethod.GET,
				null,
				returnType);

		//Assert
		assertThat(actualResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
	}

	@Test
	public void shouldCreateTask() throws Exception {
		//Act
		ParameterizedTypeReference<Task> returnType = new ParameterizedTypeReference<Task>() {
		};

		Task createTask = new Task("Practice Java", taskDate1, "Activities", Priority.HIGH, false);

		ResponseEntity<Task> actualResponse =  template.exchange(
				base.toString() + "/tasks/",
				HttpMethod.POST,
				new HttpEntity<Task>(createTask, new HttpHeaders()),
				Task.class,
				returnType);

		//Assert
		Task actualBody = actualResponse.getBody();
		testEntitiesId.add(actualBody.getId());
		assertThat(actualResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
		Task expectedTask = new Task("Practice Java", taskDate1, "Activities", Priority.HIGH, false);
		expectedTask.setId(actualBody.getId());
		assertThat(actualBody).isEqualToComparingFieldByField(expectedTask);
	}
}