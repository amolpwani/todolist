package com.todolist.ServiceTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.todolist.models.Priority;
import com.todolist.models.Task;
import com.todolist.repositories.TaskRepository;
import com.todolist.services.TaskService;

@RunWith(MockitoJUnitRunner.class)
public class TaskServiceTest {

    @InjectMocks
    private TaskService taskService;

    @Mock
    private TaskRepository taskRepositoryMock;
    
    @SuppressWarnings("deprecation")
	private Date taskDate1 = new Date(118, 1, 27);

    @Test
    public void allTask_shouldReturnEmptyWhenNoTaskFound() {
        //Arrange
        when(taskRepositoryMock.findAll()).thenReturn(Lists.emptyList());
        //Act
        List<Task> actualResult = taskService.allTask();
        //Assert
        assertThat(actualResult).hasSize(0);
    }

    @Test
    public void allTask_shouldReturnValuesWhenTaskFound() {
        //Arrange
        List<Task> task = new ArrayList<>(1);
        task.add(buildTask());
        when(taskRepositoryMock.findAll()).thenReturn(task);
        //Act
        List<Task> actualResult = taskService.allTask();
        //Assert
        assertThat(actualResult).isEqualTo(task);
    }

    @Test
    public void saveTask_shouldSaveATask() {
        //Arrange
        ArgumentCaptor<Task> taskCaptor = ArgumentCaptor.forClass(Task.class);

        when(taskRepositoryMock.save(taskCaptor.capture()))
                .thenReturn(buildTask());

        //Act
        taskService.saveTask(buildTask());

        Task expectedEntity = buildTask();
        expectedEntity.setId(1l);

        //Assert
        assertThat(taskCaptor.getValue()).isEqualToComparingFieldByField(expectedEntity);
    }

    @Test
    public void deleteTaskById_shouldInvokeDeleteOnRepository() {
        //Arrange
        ArgumentCaptor<Task> taskCaptor = ArgumentCaptor.forClass(Task.class);
        doNothing().when(taskRepositoryMock).delete(taskCaptor.capture());
        //Act
        taskService.deleteTask(buildTask().getId());
        //Assert
        assertThat(taskCaptor.equals(taskCaptor));
    }

    @Test
    public void allTask_shouldRetrieveAllTask() {
        //Arrange
        List<Task> task = new ArrayList<>(1);
        task.add(buildTask());
        when(taskRepositoryMock.findAll()).thenReturn(task);
        //Act
        List<Task> actualTask = taskService.allTask();
        //Assert
        assertThat(actualTask).isNotNull();
        assertThat(actualTask).hasSize(1);
        assertThat(actualTask.get(0).getName()).isEqualTo("Practice Java");
        assertThat(actualTask.get(0).getCategory()).isEqualTo("Activities");
    }

    private Task buildTask() {
        Task entity = new Task("Practice Java", taskDate1, "Activities", Priority.HIGH, false);
        entity.setId(1l);
        return entity;
    }
}