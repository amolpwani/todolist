package com.todolist.RepositoryTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.todolist.models.Priority;
import com.todolist.models.Task;
import com.todolist.repositories.TaskRepository;


/**
 * Created by dudekula.abedin on 1/8/2018.
 */

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = NONE)
public class TaskRepositoryTest {
	@Autowired
	private TestEntityManager entityManager;
	@Autowired
	private TaskRepository taskRepository;
	@Test
	public void directList() {

		Task task1 = new Task("JunitTesting1", new Date(), "Activities", Priority.HIGH, false);
		entityManager.persist(task1);
		entityManager.flush();
		
		Task task2 = new Task("JunitTesting2", new Date(), "Activities", Priority.HIGH, false);
		entityManager.persist(task2);
		entityManager.flush();
		
		//given
		List<Task> taskList = taskRepository.findAll();
		for (Task task : taskList) {
			if (task.getName().equals(task1.getName())) {
				//then
				Task tasktemp=taskRepository.findOne(task.getId());
				assertThat(tasktemp.getName()).isEqualTo(task1.getName());
			} else if(task.getName().equals(task2.getName())) {
				//then
				Task tasktemp=taskRepository.findOne(task.getId());
				assertThat(tasktemp.getName()).isEqualTo(task2.getName());
			}
		}
	}

	@Test
	public void taskById() {
		Task task1 = new Task("JunitTesting1", new Date(), "Activities", Priority.HIGH, false);
		entityManager.persist(task1);
		entityManager.flush();
		
		List<Task> taskList = taskRepository.findAll();
		for (Task task : taskList) {
			if(task.getName().equals(task1.getName())) {
				//then
				Task tasktemp=taskRepository.findOne(task.getId());
				assertThat(tasktemp.getName()).isEqualTo(task1.getName());
			}
		}
	}
	
	@Test
	public void updateTask() {

		Task task1 = new Task("JunitTesting1", new Date(), "Activities", Priority.HIGH, false);
		entityManager.persist(task1);
		entityManager.flush();

		List<Task> taskList = taskRepository.findAll();
		for (Task task : taskList) {
			if (task.getName().equals(task1.getName())) {
				//then
				Task tasktemp=taskRepository.findOne(task.getId());
				Task task2 = new Task("JunitTesting1", new Date(), "Activities", Priority.HIGH, false);
				BeanUtils.copyProperties(task2, taskList);
				assertThat(tasktemp.getName()).isEqualTo(task1.getName());
			}
		}
	}
	
	@Test
	public void deleteTaskById() {
		Task task1 = new Task("JunitTesting1", new Date(), "Activities", Priority.HIGH, false);
		entityManager.persist(task1);
		entityManager.flush();
		List<Task> taskList = taskRepository.findAll();
		
		for (Task task : taskList) {
			if (task.getName().equals(task1.getName())) {
				//then
				Task tasktemp = taskRepository.findOne(task.getId());
				taskRepository.delete(task);
				assertThat(tasktemp.getName()).isEqualTo(task1.getName());
			}
		}
	}
	
	@Test
	public void createTask() {
		Task task1 = new Task("JunitTesting1", new Date(), "Activities", Priority.HIGH, false);
		entityManager.persist(task1);
		entityManager.flush();
		List<Task> taskList = taskRepository.findAll();
		
		for (Task task : taskList) {
			if(task.getName().equals(task1.getName())) {
				//then
				Task tasktemp = taskRepository.findOne(task.getId());
				assertThat(tasktemp.getName()).isEqualTo(task1.getName());
			}
		}
	}
}
